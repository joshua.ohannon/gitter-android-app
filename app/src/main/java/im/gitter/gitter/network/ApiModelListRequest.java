package im.gitter.gitter.network;

import android.content.Context;

import com.android.volley.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public abstract class ApiModelListRequest<Model> extends ApiRequest<ArrayList<Model>> {
    public ApiModelListRequest(Context context, String path, Response.Listener<ArrayList<Model>> listener, Response.ErrorListener errorListener) {
        super(context, path, listener, errorListener);
    }

    @Override
    protected ArrayList<Model> parseJsonInBackground(String json) throws JSONException {
        JSONArray jsonArray = new JSONArray(json);
        ArrayList<Model> models = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            models.add(createModel(jsonArray.getJSONObject(i)));
        }

        return models;
    }

    protected abstract Model createModel(JSONObject jsonObject) throws JSONException;
}
