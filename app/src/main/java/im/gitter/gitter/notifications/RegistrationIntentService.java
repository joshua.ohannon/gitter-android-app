package im.gitter.gitter.notifications;

import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import im.gitter.gitter.network.Api;
import im.gitter.gitter.BuildConfig;

public class RegistrationIntentService extends IntentService {

    public final static String DEREGISTER_INTENT_KEY = "deregister";
    private Api api;
    private RegistrationData registrationData;
    private String projectId;

    public RegistrationIntentService() {
        super(RegistrationIntentService.class.getSimpleName());
    }

    @Override
    public void onCreate() {
        super.onCreate();

        this.api = new Api(this);
        this.registrationData = new RegistrationData(this);
        this.projectId = BuildConfig.google_project_id;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        boolean shouldDeregister = intent.getBooleanExtra(DEREGISTER_INTENT_KEY, false);

        if (shouldDeregister) {
            deregister();
        } else {
            register();
        }
    }

    private void register() {
        registrationData.wipe();
        InstanceID instanceID = InstanceID.getInstance(this);
        String id = instanceID.getId();

        try {
            String token = instanceID.getToken(projectId, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            postTokenToGitter(id, token);
            registrationData.setHasRegistered();
        } catch (InterruptedException
                |ExecutionException
                |TimeoutException
                |IOException
                |PackageManager.NameNotFoundException
                |JSONException e) {

            e.printStackTrace();
        }
    }

    private void deregister() {
        registrationData.wipe();

        try {
            InstanceID.getInstance(this).deleteToken(projectId, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void postTokenToGitter(String id, String token) throws InterruptedException, ExecutionException, TimeoutException, JSONException, PackageManager.NameNotFoundException {
        String deviceName = android.os.Build.MODEL;
        String version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

        JSONObject json = new JSONObject();
        json.put("deviceId", id);
        json.put("deviceName", deviceName);
        json.put("version", version);
        json.put("registrationId", token);

        RequestQueue queue = Volley.newRequestQueue(this);
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        queue.add(api.createJsonObjRequest(
                Request.Method.POST,
                "/v1/private/gcm",
                json,
                future,
                future
        ));

        // blocking call to keep things synchronous
        future.get(30, TimeUnit.SECONDS);
    }
}
